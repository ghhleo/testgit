package com.friday.apiService;

        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author alanho123@gmail.com
 * @version 1.0
 */

@SpringBootApplication
public class FridayApiServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FridayApiServiceApplication.class, args);
    }
}
