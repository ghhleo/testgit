package com.friday.apiService;

import com.friday.apiService.response.FulfillmentMessages;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author alanho123@gmail.com
 * @version 1.0
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fulfillmentText",
        "fulfillmentMessages"
})
public class MarketData {
    @JsonProperty("fulfillmentText")
    private String fulfillmentText;

    @JsonProperty("fulfillmentMessages")
    private FulfillmentMessages[] fulfillmentMessages;

    public String getFulfillmentText() {
        return fulfillmentText;
    }

    public void setFulfillmentText(String fulfillmentText) {
        this.fulfillmentText = fulfillmentText;
    }

    public FulfillmentMessages[] getFulfillmentMessages() {
        return fulfillmentMessages;
    }

    public void setFulfillmentMessages(FulfillmentMessages[] fulfillmentMessages) {
        this.fulfillmentMessages = fulfillmentMessages;
    }
}
