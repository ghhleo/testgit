package com.friday.apiService;

public enum PlatformEnum {

    FB("FACEBOOK"),
    SKYPE("SKYPE"),
    PC("電腦");

    private String code;

    PlatformEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public Object getName() {
        return this.toString();
    }

    public static PlatformEnum findByCode(String code) {
        for (PlatformEnum funcType : PlatformEnum.values()) {
            if (funcType.getCode().equalsIgnoreCase(code)) {
                return funcType;
            }
        }
        return null;
    }


    @Override
    public String toString() {
        return super.toString();
    }

}
