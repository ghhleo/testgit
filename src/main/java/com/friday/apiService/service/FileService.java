package com.friday.apiService.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    private  void writeToFile(StringBuilder sb) {
        /*
         * To write contents of StringBuffer to a file, use
         * BufferedWriter class.
         */

        try {
            BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("/Users/leochen/AD/index2.html")));

            //write contents of StringBuffer to a file
            bwr.write(sb.toString());

            //flush the stream
            bwr.flush();

            //close the stream
            bwr.close();

            System.out.println("Content of StringBuffer written to File.");
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public StringBuilder readFile(String path, String defaultPath)
    {
        StringBuilder contentBuilder = new StringBuilder();
        String filePath = getFilePath(path, defaultPath);
        if (null != filePath) {
            try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
            {

                String sCurrentLine;
                while ((sCurrentLine = br.readLine()) != null)
                {
                    contentBuilder.append(sCurrentLine.trim());
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
                logger.error("檔案名稱位置： {}", filePath);
                logger.error("取得檔案內容資訊發生異常： {}", e);
            }
            return contentBuilder;
        } else {
            logger.error("########取得檔案位置異常 請確認設定檔內容");
            return contentBuilder.append("取得檔案內容資訊發生異常");
        }

    }

    public String getFilePath(String path, String defaultPath) {

        File file = new File(path);
        if (file.exists()) {
            return path;
        } else {
            file = new File(defaultPath);
            if (file.exists()) {
                return defaultPath;
            }
        }

        return null;
    }

    //    private StringBuilder readLineByLineJava8()
//    {
//        StringBuilder contentBuilder = new StringBuilder();
//        try (Stream<String> stream = Files.lines( Paths.get(getFilePath()), StandardCharsets.UTF_8))
//        {
//            stream.forEach(s -> contentBuilder.append(s.trim()));
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//            logger.error("讀取檔案發生異常： {}", e);
//        }
//
//        return contentBuilder;
//    }
}
