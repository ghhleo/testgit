package com.friday.apiService.repository;

import com.friday.apiService.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findByAccount(String account);

}
