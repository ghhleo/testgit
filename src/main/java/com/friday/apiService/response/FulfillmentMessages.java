package com.friday.apiService.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "text",
        "card",
        "image",
        "quickReplies",
        "payload",
        "platform"
})
public class FulfillmentMessages {

    @JsonProperty("text")
    Map<String, String[]> text;

    @JsonProperty("card")
    Card card;

    @JsonProperty("image")
    Card image;

    @JsonProperty("quickReplies")
    Card quickReplies;

    @JsonProperty("payload")
    Payload payload;

    @JsonProperty("platform")
    String platform;

    public Map<String, String[]> getText() {
        return text;
    }

    public void setText(Map<String, String[]> text) {
        this.text = text;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Card getImage() {
        return image;
    }

    public void setImage(Card image) {
        this.image = image;
    }

    public Card getQuickReplies() {
        return quickReplies;
    }

    public void setQuickReplies(Card quickReplies) {
        this.quickReplies = quickReplies;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
