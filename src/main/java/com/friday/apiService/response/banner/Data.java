package com.friday.apiService.response.banner;

import io.swagger.annotations.ApiModelProperty;

public class Data {

    @ApiModelProperty(value = "檔案內容")
    String content;

    @ApiModelProperty(value = "檔案格式")
    String type;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
