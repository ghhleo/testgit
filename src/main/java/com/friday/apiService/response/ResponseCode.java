package com.friday.apiService.response;

public enum ResponseCode {

    SUCCESS("0000", "OK"),

    ;

    String code, message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getMessage(Object... param) {

        if (param == null) {
            return this.message;
        }

        return org.slf4j.helpers.MessageFormatter.arrayFormat(message, param).getMessage();
    }

    private ResponseCode(String code) {
        this.code = code;
        this.message = this.toString();
    }

    private ResponseCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
