package com.friday.apiService.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "title",
        "subtitle",
        "imageUri",
        "buttons"
})
public class Card {

    @JsonProperty("title")
    String title;

    @JsonProperty("subtitle")
    String subtitle;

    @JsonProperty("imageUri")
    String imageUri;

    @JsonProperty("buttons")
    Map<String, String>[] buttons;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public Map<String, String>[] getButtons() {
        return buttons;
    }

    public void setButtons(Map<String, String>[] buttons) {
        this.buttons = buttons;
    }
}
