package com.friday.apiService;

import com.friday.apiService.response.banner.Data;
import com.friday.apiService.service.FileService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author alanho123@gmail.com
 * @version 1.0
 */

@RestController
@RequestMapping("/Ad")
public class AdController {
    private static final Logger logger = LoggerFactory.getLogger(AdController.class);

    @Value("${api.Ad.file.path}")
    private String adFilePath;

    @Value("${api.Ad.DefaultFile.path}")
    private String adDefaultFilePath;

    @Autowired
    FileService fileService;

    @ApiOperation(value="取得廣告頁面資訊", notes="")
    @RequestMapping(value={""}, method=RequestMethod.GET)
    public Data getAd(HttpServletRequest request) {

        logger.info("取得廣告頁面資訊 API 來源 IP :{}", request.getRemoteAddr());
        Data data = new Data();

        StringBuilder contentBuilder = fileService.readFile(adFilePath, adDefaultFilePath);
//        writeToFile(contentBuilder);
        data.setContent(contentBuilder.toString().replace("\"","\'"));
        data.setType("html");

        return data;

    }

}