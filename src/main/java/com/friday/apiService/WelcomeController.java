package com.friday.apiService;

import com.friday.apiService.response.banner.Data;
import com.friday.apiService.service.FileService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author alanho123@gmail.com
 * @version 1.0
 */

@RestController
@RequestMapping("/welcome")
public class WelcomeController {
    private static final Logger logger = LoggerFactory.getLogger(WelcomeController.class);

    @Value("${api.welcome.file.path}")
    private String welcomeFilePath;

    @Value("${api.welcome.DefaultFile.path}")
    private String welcomeDefaultFilePath;

    @Autowired
    FileService fileService;

    @ApiOperation(value="取得Welcome Message資訊", notes="")
    @RequestMapping(value={""}, method=RequestMethod.GET)
    public Data getWelcome(HttpServletRequest request) {

        logger.info("取得Welcome Message資訊 API 來源 IP :{}", request.getRemoteAddr());
        Data data = new Data();

        StringBuilder contentBuilder = fileService.readFile(welcomeFilePath, welcomeDefaultFilePath);

        data.setContent(contentBuilder.toString().replace("\"","\'"));
        data.setType("html");

        return data;

    }

}